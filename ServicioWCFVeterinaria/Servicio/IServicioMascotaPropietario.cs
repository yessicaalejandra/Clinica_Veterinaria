﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.ServiceModel.Web;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioMascotaPropietario" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioMascotaPropietario
    {
        [OperationContract]
        [WebGet(UriTemplate = "MascotaPropietario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<mascotapropietario> getMascotaPropietarios();

        [OperationContract]
        [WebGet(UriTemplate = "MascotaPropietario/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        mascotapropietario getMascotaPropietario(int id);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "MascotaPropietario", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean AddMascotaPropietario(mascotapropietario newMascotaPropietario);


        [OperationContract]
        [WebInvoke(UriTemplate = "MascotaPropietario", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyMascotaPropietario(mascotapropietario newMascotaPropietario);

    }
}
