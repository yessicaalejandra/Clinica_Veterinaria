﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServicioWCFVeterinaria.Model

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioHistorial" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioHistorial
    {

        [OperationContract]
        [WebGet(UriTemplate = "Historial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<historial> getHistorials();
        
        [OperationContract]
        [WebGet(UriTemplate = "Historial/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        historial getHistorial(int id);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "Historial", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean AddHistorial(historial newHistorial);

        [OperationContract]
        [WebInvoke(UriTemplate = "Historial", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyHistorial(historial newHistorial);
                
    }
}
